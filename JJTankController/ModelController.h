//
//  ModelController.h
//  JJTankController
//
//  Created by Yixiu Jiang on 14/11/14.
//  Copyright (c) 2014年 JJStudio. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DataViewController;

@interface ModelController : NSObject <UIPageViewControllerDataSource>

- (DataViewController *)viewControllerAtIndex:(NSUInteger)index storyboard:(UIStoryboard *)storyboard;
- (NSUInteger)indexOfViewController:(DataViewController *)viewController;

@end

