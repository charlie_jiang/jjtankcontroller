//
//  RootViewController.h
//  JJTankController
//
//  Created by Yixiu Jiang on 14/11/14.
//  Copyright (c) 2014年 JJStudio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RootViewController : UIViewController <UIPageViewControllerDelegate>

@property (strong, nonatomic) UIPageViewController *pageViewController;

@end

